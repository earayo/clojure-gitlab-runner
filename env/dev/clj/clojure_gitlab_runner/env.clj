(ns clojure-gitlab-runner.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [clojure-gitlab-runner.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[clojure-gitlab-runner started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[clojure-gitlab-runner has shut down successfully]=-"))
   :middleware wrap-dev})
