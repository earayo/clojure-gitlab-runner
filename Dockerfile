FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/clojure-gitlab-runner.jar /clojure-gitlab-runner/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/clojure-gitlab-runner/app.jar"]
